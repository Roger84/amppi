<?php

/**
 * Custom modules module
 *
 * @link       https://www.fredericgilles.net/fg-joomla-to-wordpress/
 * @since      2.0.0
 *
 * @package    FG_Joomla_to_WordPress_Premium
 * @subpackage FG_Joomla_to_WordPress_Premium/admin
 */

if ( !class_exists('FG_Joomla_to_WordPress_Modules', false) ) {

	/**
	 * Custom modules class
	 *
	 * @package    FG_Joomla_to_WordPress_Premium
	 * @subpackage FG_Joomla_to_WordPress_Premium/admin
	 * @author     Frédéric GILLES
	 */
	class FG_Joomla_to_WordPress_Modules {

		private $modules_count = 0;

		/**
		 * Initialize the class and set its properties.
		 *
		 * @since    2.0.0
		 * @param    object    $plugin       Admin plugin
		 */
		public function __construct( $plugin ) {

			$this->plugin = $plugin;

		}

		/**
		 * Import the Joomla custom modules
		 * 
		 */
		public function import_modules() {
			$modules_count = 0;
			if ( isset($this->plugin->premium_options['skip_modules']) && $this->plugin->premium_options['skip_modules'] ) {
				return;
			}
			if ( $this->plugin->import_stopped() ) {
				return;
			}
			
			$this->plugin->log(__('Importing modules...', $this->plugin->get_plugin_name()));
			
			do_action('fgj2wp_pre_import_modules');
			
			$text_widget = new WP_Widget('text', 'Text');
			$widget_settings = $text_widget->get_settings();
			$last_widget_id = max(array_keys($widget_settings));
			
			$imported_modules_ids = $this->get_imported_modules_ids($widget_settings);
			
			$modules = $this->get_modules();
			$modules_count = count($modules);
			foreach ( $modules as $module ) {
				if ( !in_array($module['id'], $imported_modules_ids) ) { // Don't reimport an existing module
					$widget_settings = $this->add_module($module, $text_widget, $widget_settings, ++$last_widget_id);
				}
			}
			$this->plugin->progressbar->increment_current_count($modules_count);
			$this->plugin->display_admin_notice(sprintf(_n('%d module imported', '%d modules imported', $this->modules_count, $this->plugin->get_plugin_name()), $this->modules_count));
		}
		
		/**
		 * Get the imported Joomla modules IDs
		 * 
		 * @since 3.68.1
		 * 
		 * @param array $widget_settings Widgets
		 * @return array Modules IDs
		 */
		private function get_imported_modules_ids($widget_settings) {
			$modules_ids = array();
			foreach ( $widget_settings as $widget ) {
				if ( isset($widget['joomla_module_id']) ) {
					$modules_ids[] = $widget['joomla_module_id'];
				}
			}
			return $modules_ids;
		}
		
		/**
		 * Add a module
		 *
		 * @param array $module module
		 * @param WP_Widget $text_widget Text widget instance
		 * @param array $widget_settings Current widget settings
		 * @param int $id_widget ID of the widget
		 * @return array $widget_settings New widget settings
		 */
		private function add_module($module, $text_widget, $widget_settings, $id_widget) {
			$content = $module['content'];
			
			// Process the media
			if ( !$this->plugin->plugin_options['skip_media'] ) {
				$module_date = ($module['checked_out_time'] != '0000-00-00 00:00:00')? $module['checked_out_time']: date('Y-m-d H:i:s');
				$result = $this->plugin->import_media_from_content($content, $module_date);
				$post_media = array();
				if ( !empty($result['media']) ) {
					$post_media = $result['media'];
					$this->plugin->media_count++;
				}
				$content = $this->plugin->process_content($content, $post_media);
				$content = stripslashes($content);
			}

			// Update the widget_text option
			$widget_settings[$id_widget] = array(
				'title'				=> $module['title'],
				'text'				=> $content,
				'filter'			=> false,
				'visual'			=> true,
				'joomla_module_id'	=> $module['id'],
			);
			$text_widget->save_settings($widget_settings);
			
			// Update the sidebars_widgets option
			$sidebars_widgets = get_option('sidebars_widgets');
			$sidebars_widgets['wp_inactive_widgets'][] = 'text-' . $id_widget;
			update_option('sidebars_widgets', $sidebars_widgets);
			
			$this->modules_count++;
			
			// Increment the Joomla last imported module ID
			update_option('fgj2wp_last_module_id', $module['id']);
			
			return $widget_settings;
		}
		
		/**
		 * Reset the stored last module id when emptying the database
		 * 
		 */
		public function reset_last_module_id() {
			update_option('fgj2wp_last_module_id', 0);
		}
		
		/**
		 * Delete the imported modules
		 * 
		 * @since 3.68.1
		 */
		public function delete_imported_modules() {
			
			// Delete the imported text widgets
			$deleted_widget_ids = array();
			$text_widget = new WP_Widget('text', 'Text');
			$widget_settings = $text_widget->get_settings();
			$new_widget_settings = array();
			foreach ( $widget_settings as $widget_id => $widget ) {
				if ( !isset($widget['joomla_module_id']) ) {
					$new_widget_settings[$widget_id] = $widget;
				} else {
					$deleted_widget_ids[] = $widget_id;
				}
			}
			$text_widget->save_settings($new_widget_settings);
			
			// Remove the deleted widgets from their containers
			$sidebars_widgets = get_option('sidebars_widgets');
			foreach ( $sidebars_widgets as $container => $widgets ) {
				if ( is_array($widgets) ) {
					$new_widgets = array();
					foreach ( $widgets as $widget ) {
						$widget_id = preg_replace('/^text-/', '', $widget);
						if ( !in_array($widget_id, $deleted_widget_ids) ) {
							$new_widgets[] = $widget;
						}
					}
					$sidebars_widgets[$container] = $new_widgets;
				}
			}
			update_option('sidebars_widgets', $sidebars_widgets);
		}
		
		/**
		 * Get all the Joomla modules
		 * 
		 */
		protected function get_modules() {
			$modules = array();
			
			if ( $this->plugin->table_exists('modules') ) {
				$last_module_id = (int)get_option('fgj2wp_last_module_id'); // to restore the import where it left
				$prefix = $this->plugin->plugin_options['prefix'];
				$sql = "
					SELECT m.id, m.title, m.content, m.checked_out_time
					FROM ${prefix}modules m
					WHERE m.published = 1
					AND m.`module` = 'mod_custom'
					AND m.id > '$last_module_id'
					ORDER BY m.id
				";
				$sql = apply_filters('fgj2wp_get_modules_sql', $sql, $prefix);
				$modules = $this->plugin->joomla_query($sql);
			}
			return $modules;
		}
		
		/**
		 * Update the number of total elements found in Joomla
		 * 
		 * @since      3.0.0
		 * 
		 * @param int $count Number of total elements
		 * @return int Number of total elements
		 */
		public function get_total_elements_count($count) {
			if ( !isset($this->plugin->premium_options['skip_modules']) || !$this->plugin->premium_options['skip_modules'] ) {
				$count += $this->get_modules_count();
			}
			return $count;
		}
		
		/**
		 * Get the number of modules
		 * 
		 * @since      3.0.0
		 * 
		 * @return int Number of modules
		 */
		private function get_modules_count() {
			$count = 0;
			if ( $this->plugin->table_exists('modules') ) {
				$prefix = $this->plugin->plugin_options['prefix'];
				$sql = "
					SELECT COUNT(*) AS nb
					FROM ${prefix}modules
					WHERE published = 1
					AND `module` = 'mod_custom'
				";
				$sql = apply_filters('fgj2wp_get_modules_count_sql', $sql, $prefix);
				$result = $this->plugin->joomla_query($sql);
				if ( isset($result[0]['nb']) ) {
					$count = $result[0]['nb'];
				}
			}
			return $count;
		}
		
	}
}
